package com.example.myapplication

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.databinding.DishExampleBinding

class DishAdapter: RecyclerView.Adapter<DishAdapter.DishHolder>() {

    val dishList = ArrayList<Dishes>()

    class DishHolder(item: View): RecyclerView.ViewHolder(item) {
        val binding = DishExampleBinding.bind(item)
        fun bind(dish: Dishes) = with(binding){
            im.setImageResource(dish.imageId)
            tvTitle.text = dish.title
        }
        @RequiresApi(Build.VERSION_CODES.M)
        fun changeStyle(position: Int) = with(binding){
            tvTitle.setTextAppearance(
                when(position%2){
                    0 -> R.style.peaceVer1
                    else -> R.style.deathVer1
                }
            )
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DishHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.dish_example, parent, false)
        return DishHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: DishHolder, position: Int) {
        holder.bind(dishList[position])
        holder.changeStyle(position)
    }

    override fun getItemCount(): Int {
        return  dishList.size
    }
    fun addDish(dish: Dishes){
        dishList.add(dish)
        notifyDataSetChanged()
    }

}