package com.example.myapplication

import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.databinding.WebFragmentBinding



class WebFragment : Fragment() {

    lateinit var binding: WebFragmentBinding
    lateinit var viewModel: WebViewModel
    private var currentPage: String = "https://www.google.ru/"

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = WebFragmentBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this)[WebViewModel::class.java]
        val view = binding.root
        webViewSetup()
        currentPage = binding.wbWebView.originalUrl.toString()
        return view
    }

        @RequiresApi(Build.VERSION_CODES.O)
    private fun webViewSetup() = with(binding){
            wbWebView.webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(
                    view: WebView?,
                    request: WebResourceRequest?
                ): Boolean {
                    val url = request?.url.toString()
                    viewModel.savedURL.value = url
                    view?.loadUrl(url)
                    return super.shouldOverrideUrlLoading(view, request)
                }
            }

            wbWebView.apply {
                viewModel.savedURL.observe(viewLifecycleOwner) {
                    loadUrl(it)
                }
            }
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(
            true // default to enabled
        ) {
            override fun handleOnBackPressed() {
                if (binding.wbWebView.canGoBack()) binding.wbWebView.goBack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,  // LifecycleOwner
            callback
        )
    }

    companion object {

        @JvmStatic
        fun newInstance() = WebFragment()

    }
}