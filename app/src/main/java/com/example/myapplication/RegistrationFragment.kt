package com.example.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.login_fragmet.view.*
import kotlinx.android.synthetic.main.registration_fragment.view.*

class RegistrationFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.registration_fragment, container, false)

        view.button_back.setOnClickListener{ Navigation.findNavController(view).navigate(R.id.backToLogin)}

        return view
    }
}